import React from 'react';
import Layout from "./components/Layout/Layout";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import EditPages from "./containers/EditPages/EditPages";
import Pages from "./containers/Pages/Pages";

const App = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route path="/pages/admin" component={EditPages}/>
                    <Route path="/pages/:page" component={Pages}/>
                </Switch>
            </Layout>
        </BrowserRouter>
    );
};

export default App;