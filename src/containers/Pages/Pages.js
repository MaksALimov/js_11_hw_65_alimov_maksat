import React, {useEffect, useState} from 'react';
import axios from "axios";
import './Pages.css';

const Pages = ({match}) => {
    const [pages, setPages] = useState(null);

    useEffect(() => {
        const axiosData = async () => {
            const response = await axios.get(`https://js-11-static-pages-default-rtdb.firebaseio.com/pages/${match.params.page}.json`);
            setPages(response.data);
        };
        axiosData().catch(console.log);
    }, [match.params.page]);

    return pages && (
        <div className="PagesWrapper">
            <div>
                <h2 className="Title">{pages.title}</h2>
            </div>
            <div>
                <h4 className="Content">Content</h4>
                <p>{pages.content}</p>
            </div>
        </div>
    );
};

export default Pages;