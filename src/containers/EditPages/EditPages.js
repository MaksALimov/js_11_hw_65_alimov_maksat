import React, {useEffect, useState} from 'react';
import axios from "axios";
import './EditPages.css';

const EditPages = ({history}) => {
    const [select, setSelect] = useState({
        page: 'about',
    });

    const [selectedPage, setSelectedPage] = useState({
        title: '',
        content: '',
    });

    useEffect(() => {
        const axiosData = async () => {
            const response = await axios.get(`https://js-11-static-pages-default-rtdb.firebaseio.com/pages/${select.page}.json`);
            setSelectedPage(response.data);
        };

        axiosData().catch(console.log);
    }, [select]);

    const onInputChange = e => {
        const {name, value} = e.target;

        setSelectedPage(prevState => ({...prevState, [name]: value}))
    };


    const sendEditedPost = e => {
        e.preventDefault();

        const axiosData = async () => {
            await axios.put(`https://js-11-static-pages-default-rtdb.firebaseio.com/pages/${select.page}.json`, {
                title: selectedPage.title,
                content: selectedPage.content,
            });

            setSelectedPage(prevState => ({...prevState, title: '', content: ''}));
            history.replace(`/pages/${select.page}`);
        };

        axiosData().catch(console.log);
    };

    return (
        <div>
            <h2 className="EditPagesTitle">Edit pages</h2>
            <form onSubmit={sendEditedPost} className="EditPagesWrapper">
                <select name="pages" value={select.page} onChange={e => setSelect({page: e.target.value})}>
                    <option disabled>Выберите страницу</option>
                    <option value="about">About</option>
                    <option value="contacts">Contacts</option>
                    <option value="divisions">Divisions</option>
                    <option value="FAQ">FAQ</option>
                    <option value="privacy">Privacy</option>
                    <option value="Home">Home</option>
                </select>

                <label>
                    Title:
                    <input type="text" name="title" value={selectedPage.title} onChange={onInputChange}/>
                </label>
                <label>
                    Content
                    <textarea name="content" rows="20" cols="40" value={selectedPage.content} onChange={onInputChange}/>
                </label>
                <button type="submit">Save</button>
            </form>
        </div>
    );
};

export default EditPages;