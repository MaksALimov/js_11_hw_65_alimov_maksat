import {useEffect, useState} from "react";
import axios from "axios";
import {NavLink} from "react-router-dom";
import './NavPages.css';

const NavPages = () => {
    const [navPages, setNavPages] = useState([]);

    useEffect(() => {
        const axiosData = async () => {
            const response = await axios.get('https://js-11-static-pages-default-rtdb.firebaseio.com/pages.json');
            setNavPages(Object.keys(response.data));
        };

        axiosData().catch(console.log);
    }, []);

    return (
        <div className="StaticPageWrapper">
            <h1>Static pages</h1>
            <nav className="StaticPageWrapper__list">
                {navPages.map((page, index) => (
                    <NavLink to={`/pages/${page}`} key={index}>
                        <li>{page}</li>
                    </NavLink>
                ))}
                <NavLink to="/pages/admin" exact>
                    <li>Admin</li>
                </NavLink>
            </nav>
        </div>
    );
};

export default NavPages;
