import React from 'react';
import NavPages from "../NavPages/NavPages";

const Layout = ({children}) => {
    return (
        <div>
            <header>{<NavPages/>}</header>
            {children}
        </div>
    );
};

export default Layout;